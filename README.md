# OldSchool RuneScape Grand Exchange Pricing Fetcher

This is a little tool I wrote so that I could run calculations on GE prices over
time and graph the results. With this tool it is possible to import GE data
into a spreadsheet program, graph prices of different items against each other,
or whatever else you want to do with the data. The data itself is backed by a
sqlite3 database (See src/datasource.rb for the schema, and use `--help` to get
the location of the db.)

## How to use

All of the following commands assume you are in a terminal inside the cloned
repository.

To get Grand Exchange information on the blue partyhat, type:

    ./osrsge get 1042

Replace `1042` with another id of your choosing. To get the id of an item,
[search for the item on the GE website](http://services.runescape.com/m%3Ditemdb_oldschool/),
then mouse over the item to see the url. Look for `?obj=` and the item id will follow.
([This is a sample url](http://services.runescape.com/m%3Ditemdb_oldschool/Blue_partyhat/viewitem?obj=1042))


To get a tabular format of pricing history of multiple items, first you must
add the items you want to a local cache, then run the `price-table` command.
Later, you can run `update` to update the local cache for the items you have
added. The output is designed for copying into your favorite spreadsheet
program.

    ./osrsge -v track 436 438 440 442 444 447 449 451 #IDs for different ore
    ./osrsge price-table  #hint: pipe to `pbcopy` or a similar command

    # ...after time passes

    ./osrsge -v update
    ./osrsge price-table

You can the cached data (price and trade volume for at least the last 180 days)
of a single item as follows:

    ./osrsge show 1042
    ./osrsge show blue partyhat #can specify item by name in "show" and "untrack"
    ./osrsge show bluphat #you can leave letters out so long as the match is unambiguous

For other commands and more detailed help, run the following

    ./osrsge --help

## Questions, Bugs, Concerns

If you encounter any issues, or if documenation is unclear, do not hesitate to
[open an issue](https://github.com/win93/osrs-grandexchange-cli/issues). Cheers.
