# Copyright (c) 2019 Alex Gittemeier <gittemeier.alex@gmail.com>
PRODUCT = "osrsge: OldSchool RuneScape Grand Exchange Data Fetcher"
VERSION = "1.0"
DISCLAIMER = <<-EOF
Disclaimer: This is an unofficial tool that is not affiliated with or endorsed
by Jagex, Ltd. Use of this tool is without warranty, express or implied,
including the accuracy and availablity of provided data, and is provided on an
'as-is' basis.

Please send bug reports to:

https://github.com/win93/osrs-grandexchange-cli/issues
EOF

require 'uri'
require_relative 'datasource'
require_relative 'geitem'

def assert_has_args(usage)
	if ARGV.empty?
		$stderr.puts "Usage: osrsge #{usage}"
		exit 1
	end
end

def assert_no_args(usage)
	if !ARGV.empty?
		$stderr.puts "Usage: osrsge #{usage}"
		exit 1
	end
end

def parse_item_spec(db, argv)
	item_spec = argv.join(" ")
	begin
		Integer(item_spec)
	rescue ArgumentError
		item_ids = db.lookup_item(item_spec)
		if item_ids.count > 1
			$stderr.puts "osrsge: Multiple matches for \"#{item_spec}\""
			item_ids.each do |id|
				$stderr.puts "#{id}: #{db.get_item_name(id)}"
			end
			exit 1
		elsif item_ids.empty?
			$stderr.puts "osrsge: \"#{item_spec}\" not found or not tracked."
			$stderr.puts "osrsge: Go to http://services.runescape.com/m%3Ditemdb_oldschool/results?query=#{URI.escape(item_spec)}"
			$stderr.puts "osrsge: then mouse over the item to get the objid=??? from the url, then run `osrsge track <id>` to track"
			exit 1
		else
			item_ids[0]
		end
	end
end

DB_LOCATION = File.join(__dir__, "..", "ge.db")
db = DataSource.new(DB_LOCATION)

#XXX: really simple options parsing
opts_verbose = false
while ARGV[0] && ARGV[0][0] == '-'
	opt = ARGV.shift
	break if opt == '--'
	case opt
	when '-v', '--verbose'
		opts_verbose = true
	when '--help'
		puts <<-EOF
#{PRODUCT}

Usage: osrsge [-v] <subcommand>

  -v, --verbose: print what modifications were made to the local cache

"subcommand" is one of the following:
 - get <item id>: Fetch Grand Exchange info from Jagex and display it in the
                  the manner described in the subcommand "show". This does not
                  update the local cache.

 - track <item ids>: Add items of interest by their item ids.
   (synonym: add)    This fetches from Jagex the price/volume history for this
                     item for the last 180 days and saves it to a local cache.

                     To get an item id for an item go to:

                     http://services.runescape.com/m%3Ditemdb_oldschool/

                     search for the item, then grab the id from the url.

                     Example:
                     For a blue partyhat, the search will give the url:

                     http://services.runescape.com/m%3Ditemdb_oldschool/Blue_partyhat/viewitem?obj=1042

                     You can run `osrsge track 1042` to add Blue partyhat.

 - update: Fetch any new pricing/volume information for Jagex and add it to
           the local cache. The history from Jagex goes back 180 days, but this
           command will retain any older data already in the cache.

 - show <item>: Display a single item's data in the local cache. You may specify
                the item id or its fuzzy matched name. The output lists the
                item's name, its id, and its price (in gp) and trading volume
                for each day.

 - list: List all the items in the local cache.

 - price-table: Print a tab separated table of the price history of all items
                in the local cache. This command is formatted for copying into a
                spreadsheet program, not for nice looking console output.

 - untrack <item>:            Remove the specified item from the local cache.
   (synonyms: remove, delete) You may specify the item id or fuzzy matched name.

Local Cache

	The price/volume data is stored in a sqlite3 database located here:

	#{File.realpath(DB_LOCATION)}

  This location is hard coded at the moment D:

Fuzzy Matching

  Item names on the command-line (hereafter known as the item spec) are matched
  to items whose name contains the letters of the item spec, in that order.
  (Sort of like how IDE autocomplete works.) For example "cball" will match
  "Cannonball" because 'c', 'b', 'a', 'l', 'l' appear in that order in
  "Cannonball". Matching is case-insensitive, and any non alphanumeric character
	is ignored.

#{DISCLAIMER}
EOF
	exit
	when '--version'
		puts <<-EOF
#{PRODUCT}
Version #{VERSION}

Copyright (C) 2019 Alex Gittemeier <gittemeier.alex@gmail.com>

#{DISCLAIMER}
EOF
	exit
	else
		$stderr.puts "Invalid option #{opt}"
		ARGV.clear #cause usage to be printed due to missing subcommand
	end
end

subcommand = ARGV.shift
case subcommand
when "get"
	assert_has_args("get <item_id>")
	item_id = begin
		Integer(item_spec)
	rescue ArgumentError
		$stderr.puts "osrsge: Expecting a numeric id, not \"#{item}\""
		exit 1
	end

	item = GEItem.fetch(item_id)
	if item
		puts "Item:\t#{item.name}\t#{item.id}"
		puts "Date\tPrice\tVolume"
		item.trade_info.each{|info| puts "#{info.date.strftime("%Y-%m-%d")}\t#{info.price}\t#{info.volume}" }
	else
		$stderr.puts "osrsge: item #{item_spec} doesn't exist."
		exit 1
	end
when "track", "add"
	assert_has_args("#{subcommand} <item_id> ...")

	rc = 0
	while item_spec = ARGV.shift
		item_id = begin
			Integer(item_spec)
		rescue ArgumentError
			$stderr.puts "osrsge: Expecting a numeric id, not \"#{item}\". Skipping"
			rc = 1
			next
		end

		item = GEItem.fetch(item_id)
		if item
			item.store_to(db)
			puts "Now tracking #{item.id}: #{item.name} (Currently #{item.trade_info[-1].price}gp)" if opts_verbose
		else
			$stderr.puts "osrsge: item with id #{item_id} doesn't exist. Skipping"
			rc = 1
		end
	end
	exit rc

when "show"
	assert_has_args("show <item_id|item_name>")
	item_id = parse_item_spec(db, ARGV)

	item = GEItem.fetch_cached(db, item_id)
	if item
		puts "Item:\t#{item.name}\t#{item.id}"
		puts "Date\tPrice\tVolume"
		item.trade_info.each{|info| puts "#{info.date.strftime("%Y-%m-%d")}\t#{info.price}\t#{info.volume}" }
	else
		$stderr.puts "osrsge: item #{item_spec} not tracked. Use `osrsge track #{item_spec}` to track."
		exit 1
	end

when "list"
	assert_no_args("list")

	db.get_item_ids.each do |id|
		puts "#{id}. #{db.get_item_name(id)}"
	end

when "price-table"
	assert_no_args("price-table")

	items = db.get_item_ids.map{|id| GEItem.fetch_cached(db, id) }
	print "Date"
	items.each {|item| print "\t#{item.name}" }
	puts

 	#stored row major, init new keys with an array
	table = Hash.new {|hash, key| hash[key] = []}
	items.each_with_index do |item, col_idx|
		item.trade_info.each do |info|
			table[info.date][col_idx] = info.price
		end
	end

	table.keys.sort.each do |date|
		print date.strftime("%Y-%m-%d")
		table[date].each {|col| print "\t#{col}" }
		puts
	end

when "update"
	assert_no_args("update")

	rc = 0
	db.get_item_ids.each do |id|
		item = GEItem.fetch(id)
		if item
			puts "Updated #{item.name} (id #{item.id}) (Currently #{item.trade_info[-1].price}gp)" if opts_verbose
			item.store_to(db)
		else
			$stderr.puts "osrsge: #{db.get_item_name(id)} (id #{id}) no longer exists! Consider running `osrsge remove #{id}`"
			rc = 1
		end
	end
	exit rc

when "untrack", "delete", "remove"
	assert_has_args("#{subcommand} <item_id|item_name>")
	item_id = parse_item_spec(db, ARGV)
	puts "Deleting #{db.get_item_name(item_id)} (id #{item_id})" if opts_verbose
	db.delete_item(item_id)

else
	$stderr.puts "Invalid subcommand: #{subcommand}" if subcommand
	$stderr.puts "Usage: osrsge [-v] <subcommand>"
	$stderr.puts "Subcommands: get, track|add, show, list, price-table, update, untrack|delete|remove"
	$stderr.puts
	$stderr.puts "Type `osrsge --help` for more help"
	exit 1
end
