# Copyright (c) 2019 Alex Gittemeier <gittemeier.alex@gmail.com>
require 'sqlite3'
require_relative 'geitem'

class DataSource
	def initialize(filename)
		@db = SQLite3::Database.open(filename)

		@db.execute 'CREATE TABLE IF NOT EXISTS item (
			id INTEGER PRIMARY KEY,
			name TEXT NOT NULL)'
		@db.execute 'CREATE TABLE IF NOT EXISTS trade_info (
			item_id INTEGER REFERENCES item(id),
			date INTEGER,
			price INTEGER NOT NULL,
			volume INTEGER NOT NULL,
			PRIMARY KEY (item_id, date))'
	end

	def add_item(id, name)
		@db.transaction do
			@db.execute('INSERT OR IGNORE INTO item (id, name) VALUES (?, ?)', [id, name])
			@db.execute('UPDATE item SET name = ? WHERE id = ?', [name, id])
		end
	end

	def get_item_ids
		@db.execute('SELECT id FROM item').map {|row| row[0] }
	end

	def get_item_name(id)
		@db.get_first_value('SELECT name FROM item WHERE id = ?', [id])
	end

	def delete_item(id)
		@db.transaction do
			@db.execute('DELETE FROM trade_info WHERE item_id = ?', [id])
			@db.execute('DELETE FROM item WHERE id = ?', [id])
		end
	end

	def lookup_item(item_spec)
		item_ids = []
		item_spec = item_spec.downcase.gsub(/[^a-z0-9]/, "")
		item_spec = Regexp.new(item_spec.chars.join('.*'))
		@db.execute('SELECT id, name from item') do |id, name|
			item_ids << id if name.downcase =~ item_spec
		end
		item_ids
	end

	def add_trade_info(item_id, date, price, volume)
		@db.transaction do
			@db.execute('INSERT OR IGNORE INTO trade_info (item_id, date, price, volume) VALUES (?, ?, ?, ?)', [item_id, date.to_i, price, volume])
			@db.execute('UPDATE trade_info SET price = ?, volume = ? WHERE item_id = ? AND date = ?', [price, volume, item_id, date.to_i])
		end
	end

	def get_trade_info(item_id)
		@db.execute('SELECT date, price, volume FROM trade_info WHERE item_id = ?', [item_id]).map do |date, price, volume|
			TradeInfo.new(Time.at(date).utc, price, volume)
		end
	end
end
