# Copyright (c) 2019 Alex Gittemeier <gittemeier.alex@gmail.com>
require 'nokogiri'
require 'open-uri'

TradeInfo = Struct.new(:date, :price, :volume)
GEItem = Struct.new(:id, :name, :trade_info) do
	def self.fetch(item_id)
		uri = "http://services.runescape.com/m=itemdb_oldschool/viewitem?obj=#{item_id}"
		chart_data_re = /average180\.push\(\[new Date\('([^']+)'\), (\d+),.*\n.*trade180\.push\(\[new Date\('\1'\), (\d+)\]/

		doc = Nokogiri::HTML(open(uri))

		return nil if doc.at('.content.error')
		title = doc.at('title').text
		chart_data = doc.at('.stats + script').text

		item_name = title.match(/\A([^-]*) -/)[1]
		trade_info = chart_data.scan(chart_data_re).map do |date, price, volume|
			year, month, day = date.match(%r{(\d+)/(\d+)/(\d+)}).captures
			TradeInfo.new(Time.utc(year, month, day), Integer(price), Integer(volume))
		end
		GEItem.new(item_id, item_name, trade_info)
	end

	def store_to(db)
		db.add_item(self.id, self.name)
		self.trade_info.each do |row|
			db.add_trade_info(self.id, row.date, row.price, row.volume)
		end
	end

	def self.fetch_cached(db, item_id)
		item_name = db.get_item_name(item_id)
		return nil if !item_name
		GEItem.new(item_id, item_name, db.get_trade_info(item_id))
	end
end
